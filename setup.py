#!/usr/bin/env python
from setuptools import setup

setup(
    name="send-matrix-message",
    version="1.2.9",
    author="kmogged",
    description="Simple command and library for sending a matrix message to a room",
    entry_points={
        "console_scripts": [
            "send-matrix-message=send_matrix_message.send_matrix_message:main"
        ]
    },
)
