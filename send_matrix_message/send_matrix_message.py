#!/usr/bin/python3

import requests
import json
import argparse
import configparser
import os
import sys
from time import sleep
from hashlib import md5
from pathlib import Path


class Matrix:
    config = None
    token = None
    verbose = False
    dry_run = False

    def get_access_token(
        self, username=None, password=None, home_server=None, retry=10
    ):
        if not home_server:
            home_server = self.config["default"].get("home_server")
        if not username:
            username = self.config["default"].get("username")
        if not password:
            password = self.config["default"].get("password")
        try:
            response = requests.post(
                "https://" + home_server + "/_matrix/client/r0/login",
                json={
                    "user": username,
                    "password": password,
                    "type": "m.login.password",
                },
            )
        except requests.exceptions.ConnectionError as e:
            if retry == 0:
                raise Exception(
                    "Unable to connect and retrieve access token from Matrix server"
                )
            sleep(retry)
            return self.get_access_token(username, password, home_server, retry - 1)

        if self.verbose:
            print("INFO: response status " + str(response.status_code))
            print("INFO: response reason " + response.reason)
            print("INFO: response text " + response.text)
        if "access_token" in response.json():
            return response.json()["access_token"]

        if self.verbose:
            print("WARNING: access token not received in response")
        if retry == 0:
            raise Exception("Unable to retrieve access token from Matrix server")
        sleep(retry)
        return self.get_access_token(username, password, home_server, retry - 1)

    def add_header(self, text, header):
        return header + "\n" + text

    def add_footer(self, text, footer):
        if not text.endswith("\n"):
            text = text + "\n"
        return text + footer

    def cache_message(
        self,
        message_data,
    ):
        crypt = md5()
        crypt.update(json.dumps(message_data).encode())

        path = Path.home() / Path(".cache") / Path("send_matrix_message")
        path.mkdir(mode=0o700, exist_ok=True)

        with open(path / crypt.hexdigest(), "w") as file:
            json.dump(message_data, file)

    def send_cached_messages(self, args):
        path = Path.home() / Path(".cache") / Path("send_matrix_message")

        for item in path.glob("*"):
            lock_file = item.with_suffix(".lock")
            if lock_file.exists():
                continue
            lock_file.touch()
            print("INFO: processing cached message {}".format(item))
            with open(item, "r") as file:
                message_data = json.load(file)
            item.unlink()
            self.post_json_message(args, message_data)
            lock_file.unlink()

    def post_json_message(self, args, message_data):
        self.post_message(
            message_data.get("text"),
            message_data.get("header"),
            message_data.get("footer"),
            message_data.get("msg_type"),
            message_data.get("home_server"),
            message_data.get("room"),
            args,
        )

    def post_message(
        self,
        text,
        header=None,
        footer=None,
        msg_type="m.notice",
        home_server=None,
        room=None,
        args=None,
    ):
        if not home_server:
            home_server = self.config["default"].get("home_server")
        if not room:
            room = self.config["default"].get("room")
        if not self.token:
            try:
                self.token = self.get_access_token()
            except Exception as e:
                print("ERROR: unable to get token: {}".format(e))
                if args and not args.no_cache:
                    print("INFO: caching message for later delivery")
                    message_data = {
                        "text": text,
                        "header": header,
                        "footer": footer,
                        "msg_type": msg_type,
                        "home_server": home_server,
                        "room": room,
                    }
                    if self.dry_run:
                        print("INFO: dry run, not performing any actions")
                        return
                    self.cache_message(message_data)
                return

        if header:
            text = self.add_header(text, header)
        if footer:
            text = self.add_footer(text, footer)

        plaintext_body = text
        html_body = text.replace("\n", "<br/>")

        if self.verbose:
            print("INFO: plaintext message: \n" + plaintext_body)
            print("INFO: html message: \n" + html_body)

        if self.verbose:
            print(
                "INFO: sending message of type "
                + msg_type
                + " to "
                + home_server
                + " room "
                + room
            )

        if self.dry_run:
            print("INFO: dry run, not performing any actions")
            return
        response = requests.post(
            "https://"
            + home_server
            + "/_matrix/client/r0/rooms/"
            + room
            + "/send/m.room.message?access_token="
            + self.token,
            json={
                "msgtype": msg_type,
                "body": plaintext_body,
                "format": "org.matrix.custom.html",
                "formatted_body": html_body,
            },
        )
        if self.verbose:
            print("INFO: response status " + str(response.status_code))
            print("INFO: response reason " + response.reason)
            print("INFO: response text " + response.text)
        return response

    def get_config(self, paths):
        config = configparser.ConfigParser()
        for path in paths:
            config_path = os.path.normpath(path)
            if os.path.exists(config_path):
                config.read(config_path)
                return config
        return None

    def __init__(self):
        paths = [
            os.path.join(
                os.path.expanduser("~"), ".config", "send-matrix-message.conf"
            ),
            "/etc/send-matrix-message/send-matrix-message.conf",
        ]
        self.config = self.get_config(paths)
        if self.config is None:
            print(
                "ERROR: configuration file not accessible in following locations:",
                str(paths),
            )
            exit(1)


def main():
    parser = argparse.ArgumentParser(
        description="Send matrix messages via shell",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "--verbose", "-v", help="Displays verbose output", action="store_true"
    )
    parser.add_argument(
        "--dry-run",
        "-n",
        help="Only print the action, but do not perform anything",
        action="store_true",
    )
    parser.add_argument(
        "--message-type",
        "-m",
        help="Specify the type of message to send",
        default="m.text",
        choices=[
            "m.text",
            "m.image",
            "m.audio",
            "m.video",
            "m.location",
            "m.emote",
            "m.notice",
        ],
    )
    parser.add_argument(
        "--header", help="Append text to the header if there is body content"
    )
    parser.add_argument(
        "--footer", help="Append text to the footer if there is body content"
    )
    parser.add_argument(
        "--room", help="Specify a room other than the default in the configuration"
    )
    parser.add_argument(
        "--no-cache",
        help="Do not cache messages for later sending when server is available",
        action="store_true",
    )

    args = parser.parse_args()

    app = Matrix()
    app.verbose = args.verbose
    app.dry_run = args.dry_run

    data = sys.stdin.read()

    if data and data.strip() != "":
        app.post_message(
            data, args.header, args.footer, args.message_type, None, args.room, args
        )
    app.send_cached_messages(args)
    exit(0)


if __name__ == "__main__":
    main()
